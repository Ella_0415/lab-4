public class Bunny
{
	private String name;
	private String favoriteFood;
	private boolean behaviour;
	private int energyLv;
	//getter methods for all the fields
	public String getName()
	{
		return this.name;
	}
	public String getFavoriteFood()
	{
		return this.favoriteFood;
	}
	public boolean getBehaviour()
	{
		return this.behaviour;
	}
	public int getEnergyLv()
	{
		return this.energyLv;
	}
	//constructor
	public Bunny(String bunnyName, String bunnyFood, boolean bunnyBehaviour)
	{
		this.name = bunnyName;
		this.favoriteFood = bunnyFood;
		this.behaviour = bunnyBehaviour;
	}
	//setter methods for all the fields

	public void setFavoriteFood(String bunnyFood)
	{
		this.favoriteFood = bunnyFood;
	}
	public void setBehaviour(boolean bunnyBehaviour)
	{
		this.behaviour = bunnyBehaviour;
	}
	public void setEnergyLv(int energy)
	{
		this.energyLv = energy;
	}
	//instance methods
	public void bunnyJump(String bunnyName)
	{
		System.out.println(bunnyName + " has successfully jumped");
	}
	
	public void feedBunny(boolean bunnyBehaviour, String bunnyFood)
	{
		System.out.println("is bunny nice? ");
		if(bunnyBehaviour = false)
		{
			System.out.println("Bad bunny! You only get some hay!");
		}
		else
		{
			System.out.println("Good bunny! You can have some " + bunnyFood);
		}
	}
	
	public int bunnySleep(int jumpCount)
	{
		if(jumpCount > 0)
		{
			this.energyLv = jumpCount - (2 * jumpCount);
		}
		return this.energyLv;
	}
}