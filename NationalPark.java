import java.util.Scanner;
public class NationalPark
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Bunny[] fluffle = new Bunny[4];
		
		for(int i = 0; i < fluffle.length; i++)
		{
			System.out.println("What is your bunny's name?");
			String bunnyName = scan.next();
			System.out.println("What is your bunny's favorite food?");
			String bunnyFood = scan.next();
			System.out.println("true or false your bunny is nice");
			boolean bunnyBehaviour = scan.nextBoolean();
			fluffle[i] = new Bunny(bunnyName, bunnyFood, bunnyBehaviour);
			
		}
		
		System.out.println("Your Bunny's favorite food is: " + fluffle[3].getFavoriteFood());
		System.out.println("Is this correct? If yes rewrite the same answer, if no change the answer");
		fluffle[3].setFavoriteFood(scan.next());
		System.out.println("This is your bunny's favorite food: " + fluffle[3].getFavoriteFood());
		
		System.out.println("The following states wheter your bunny is nice, true, or not, false: " + fluffle[3].getBehaviour());
		System.out.println("Is this the correct input? if yes write it again, if no change the answer");
		fluffle[3].setBehaviour(scan.nextBoolean());
		System.out.println("The following states wheter your bunny is nice, true, or not, false:" + fluffle[3].getBehaviour());
		
		System.out.println("The last bunny's information: ");
		System.out.println("Name: " + fluffle[3].getName());
		System.out.println("Favorite food: " + fluffle[3].getFavoriteFood());
		System.out.println("It's a nice bunny: " + fluffle[3].getBehaviour());
		
		Bunny bun = new Bunny("bibi", "lettuce", true);
		System.out.println("The first bunny has something to show us!");
		bun.bunnyJump(fluffle[0].getName());
		System.out.println("Let's see if the bunny has been nice!");
		bun.feedBunny(fluffle[0].getBehaviour(), fluffle[0].getFavoriteFood());
		
		
		System.out.println("How many times did your bunny jump today?");
		int jumpAmount = scan.nextInt();
		System.out.println("Bunny's energy level is: " + fluffle[1].bunnySleep(jumpAmount));
		
	}
}